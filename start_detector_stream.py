import MotionDetectorStream

url = "rtsp://"

d = MotionDetectorStream.MotionDetectorStream(url)

d.coord = (
    [246, 246, 282, 277, 'STOYANKA',],
    [336, 352, 370, 451, 'DOROGA',],
)

d.weightAlarm = 50
d.lightingAlarm = 10
d.outputDir = 'out-live/'
d.startDetector()

