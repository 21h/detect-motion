import MotionDetector
import numpy as np
import cv2
from datetime import datetime, timedelta

class MotionDetectorStream(MotionDetector.MotionDetector):

    def __init__(self, stream):
        self.file = stream
        self.cap=cv2.VideoCapture(stream)
        self.motionDetected = False
        self.motionTimestampStart = False
        self.motionTimestampStop = False
        self.motionImages = []

    def startDetector(self):
        cap = self.cap

        for region in self.coord:
            region += [[0, 0, 0]]

        # init variables
        video = False
        videoTimer=300
        videoTimerCounter=0

        while (cap.isOpened()):
            ret, frame = cap.read()
            # draw rectangle on video, you can turn off this code
            for region in self.coord:
                p1 = (region[0], region[1])
                p2 = (region[2], region[3])       
                cv2.rectangle(frame, p1, p2, color=(0, 255, 0), thickness=1)    

            for region in self.coord:
                imgROI = self.cropRegionOfInterest(frame, region)
                p1 = (region[0], region[1])
                p2 = (region[2], region[3])       
                region[5][0] = region[5][1]
                region[5][1] = region[5][2]
                region[5][2] = cv2.cvtColor(imgROI, cv2.COLOR_BGR2GRAY)

                diffImage = self.findDifference(region[5])

                timeStamp = datetime.now()

                if self.getWeight(diffImage, self.lightingAlarm) > self.weightAlarm:
                    videoTimerCounter = 0
                    if self.motionDetected == False:
                        self.motionDetected = True
                        self.motionTimestampStart = timeStamp
                        
                        # save motion video
                        fourcc = cv2.VideoWriter_fourcc(*'avc1')
                        height, width, layers = frame.shape
                        video = cv2.VideoWriter(self.outputDir + '{} zone {}.avi'.format(self.motionTimestampStart, region[4]),fourcc,29.0,(width,height))

                    cv2.rectangle(frame, p1, p2, color=(0, 0, 255), thickness=2)

                    name = self.outputDir + "%s - %s.jpg" % (timeStamp, region[4])
                    #cv2.imwrite(name, frame)
                    
                    #print('Movement! time {}, zone {}'.format(timeStamp, region[4]))
                else:
                    if video != False and videoTimerCounter < videoTimer:
                        videoTimerCounter += 1
                    if video != False and videoTimerCounter == videoTimer: 
                        videoTimerCounter = 0
                        video.release()
                        self.motionDetected = False
                        self.motionTimestampStart = False
                        self.motionTimestampStop = False
                        video = False
                    cv2.rectangle(frame, p1, p2, color=(0, 255, 0), thickness=1)

            if self.motionDetected and videoTimerCounter<=videoTimer:
                #print('{} - {}, zone {}'.format(self.motionTimestampStart, self.motionTimestampStop, region[4]))
                video.write(frame)

            self.drawVideo(frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()

