import numpy as np
import cv2
from datetime import datetime, timedelta

class MotionDetector:

    def __init__(self, file):
        # set region of interest (ROI)
        self.coord = []

        #minimal lighting. lower = more sensitive
        self.lightingAlarm = 30

        #how much lighted pixels. lower = more sensitive
        self.weightAlarm = 250

        #how much skip frames while processing video
        #higher = less chances to find
        self.skipFrames = 5

        #balanceSkipped = false, fast and furious:
        #6 skipped, 1 processed, 6 skipped, 1 processed
        #balanceSkipped = true, slower and sensible:
        #6 skipped, 6 processed, 6 skipped, 6 processed
        self.balanceSkipped = False

        #if you want to limit processing to specific frame
        self.stopFrame = -1

        #where to save frames
        self.outputDir = ''

        #some internals
        self.file = file
        self.cap=cv2.VideoCapture(file)
        
    def getFPS(self):
        return int(self.cap.get(cv2.CAP_PROP_FPS))
    def getFramesCount(self):
        return int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))

    def cropRegionOfInterest(self, frame, rect):
        roi = frame[rect[1]:rect[3], rect[0]:rect[2]]
        #cv2.imshow(rect[4], roi)
        return roi

    def findDifference(self, diffImages):
        t0 = diffImages[0]
        t1 = diffImages[1]
        t2 = diffImages[2]

        d1 = cv2.absdiff(t2, t1)
        d2 = cv2.absdiff(t1, t0)
        return cv2.bitwise_and(d1, d2)

    def getWeight(self, diffImage, lightingAlarm):
        weightROI = 0
        height, width = diffImage.shape
        for y in range(height):
            for x in range(width):
                if diffImage[y,x] > lightingAlarm:
                    weightROI += 1
        return weightROI

    def framesToTime(self, frameNum, fps):
        secs = round(frameNum / fps)
        sec = timedelta(seconds=secs)
        d = datetime(1, 1, 1) + sec
        return "%02d:%02d:%02d" % (d.hour, d.minute, d.second)

    def drawVideo(self, frame):
        for region in self.coord:
            p1 = (region[0], region[1])
            p2 = (region[2], region[3])
            #cv2.rectangle(frame, p1, p2, color=(0, 255, 0), thickness=1)
        cv2.imshow('Video', frame)

    def startDetector(self):
        framesCount = self.getFramesCount()
        cap = self.cap

        frameNum = 0
        frameSkip = 0
        frameNext = 0

        for region in self.coord:
            region += [[0, 0, 0]]

        if self.balanceSkipped:

            while(cap.isOpened()):
                frameNum += 1
                if frameNum >= framesCount:
                    break
                else:
                    if frameNum == self.stopFrame:
                        break
                    else:
                        ret, frame = cap.read()

                if frameSkip <= self.skipFrames:
                    frameSkip += 1
                    continue
                else:
                    frameNext += 1
                    if frameNext >= self.skipFrames:
                        frameNext = 0
                        frameSkip = 0

                # draw rectangle on video, you can turn off this code
                self.drawVideo(frame)

                for region in self.coord:
                    imgROI = self.cropRegionOfInterest(frame, region)
                    region[5][0] = region[5][1]
                    region[5][1] = region[5][2]
                    region[5][2] = cv2.cvtColor(imgROI, cv2.COLOR_BGR2GRAY)

                    diffImage = self.findDifference(region[5])
                    #cv2.imshow(region[4] + 'DIFF', diffImage)

                    if self.getWeight(diffImage, self.lightingAlarm) > self.weightAlarm:
                        print('Movement! Frame {}, time {}, zone {}'.format(frameNum, self.framesToTime(frameNum, self.getFPS()), region[4]))
                        name = self.outputDir + "%s - frame %d - %s.jpg" % (region[4], frameNum, self.framesToTime(frameNum, self.getFPS()))
                        cv2.imwrite(name, frame)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        else:

            while (cap.isOpened()):
                frameNum += 1
                if frameNum >= framesCount:
                    break
                else:
                    if frameNum == self.stopFrame:
                        break
                    else:
                        ret, frame = cap.read()

                if frameSkip <= self.skipFrames:
                    frameSkip += 1
                    continue
                else:
                    frameSkip = 0

                # draw rectangle on video, you can turn off this code
                self.drawVideo(frame)

                for region in self.coord:
                    imgROI = self.cropRegionOfInterest(frame, region)
                    region[5][0] = region[5][1]
                    region[5][1] = region[5][2]
                    region[5][2] = cv2.cvtColor(imgROI, cv2.COLOR_BGR2GRAY)

                    diffImage = self.findDifference(region[5])
                    #cv2.imshow(region[4] + 'DIFF', diffImage)

                    if self.getWeight(diffImage, self.lightingAlarm) > self.weightAlarm:
                        print('Movement! Frame {}, time {}, zone {}'.format(frameNum, self.framesToTime(frameNum, self.getFPS()), region[4]))
                        name = self.outputDir + "%s - frame %d - %s.jpg" % (region[4], frameNum, self.framesToTime(frameNum, self.getFPS()))
                        cv2.imwrite(name, frame)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

        cap.release()
        cv2.destroyAllWindows()

