import MotionDetector

files = [
 '20180807180001-20180807210000.mp4',
 '20180807180001-20180807210000_1.mp4',
 '20180807180001-20180807210000_2.mp4',
 '20180807210000-20180807235951.mp4',
 '20180807210000-20180807235951_1.mp4',
 '20180807210000-20180807235951_2.mp4',
 '20180807210000-20180807235951_3.mp4',
 '20180807210000-20180807235951_4.mp4',
 '20180808000001-20180808042000.mp4',
 '20180808003004-20180808042500_1.mp4',
 '20180808003004-20180808042500_2.mp4',
 '20180808003004-20180808042500_3.mp4',
 '20180808003004-20180808042500_4.mp4',
 '20180808003004-20180808042500_5.mp4',
 '20180808003004-20180808042500_6.mp4',
 '20180808003004-20180808042500.mp4'
]

for file in files:
    try:
        d = MotionDetector.MotionDetector('/media/Sklad/'+file)

        # d.coord = (
        #    [349, 206, 515, 325, 'UAZ ZAD'],
        #    [212, 510, 424, 581, 'UAZ PERED'],
        #    [273, 206, 348, 325, 'UAZ BOK LEV'],
        #    [439, 371, 510, 460, 'UAZ-NIVA'],
        #    [516, 230, 726, 329, 'NIVA ZAD'],
        #    [481, 493, 680, 573, 'NIVA PERED'],
        #    [667, 357, 742, 442, 'NIVA BOK PRAV']
        # )

        d.coord = (
            [484, 482, 558, 558, 'STOYANKA',],
            #[293, 650, 390, 881, 'DOROGA',],
        )

        d.weightAlarm = 200
        d.lightingAlarm = 20
        d.skipFrames = 3
        d.balanceSkipped = False
        d.outputDir = 'out/'
        print("Processing: ", d.file)
        print("FPS:", d.getFPS(),
            "Frames count:", d.getFramesCount(),
            "Length:", d.framesToTime(d.getFramesCount(), d.getFPS()))
        d.startDetector()
    except Exception as e:
        print(e)


# Sample code

# d = MotionDetector('/media/Documents/tmp/detect_motion/20180320112608-20180320160500_2.mp4')
#
# d.coord = (
#     [267, 800, 373, 869, 'zone 1'],
#     [254, 663, 383, 727, 'zone 2']
# )
# d.weightAlarm = 250
# d.lightingAlarm = 30
# d.skipFrames = 6
# d.stopFrame = 1000
# d.balanceSkipped = False
# d.outputDir = 'out/'
# print("Processing: ", d.file)
# print("FPS:", d.getFPS(),
#       "Frames count:", d.getFramesCount(),
#       "Length:", d.framesToTime(d.getFramesCount(), d.getFPS()))
# d.startDetector()